require "ini"

struct Config
    def self.options
       INI.parse(File.read("./config/options.ini"))
    end
end